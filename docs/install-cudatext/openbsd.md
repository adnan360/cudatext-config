# How to Install CudaText on OpenBSD

Tested on OpenBSD 7.0 (amd64) with CudaText 1.146.2.0.

Unfortunately I don't know of any custom OpenBSD port (a way to build a package from sources) exist for CudaText. So the easiest way would be to install the binaries manually.

## Step 1: Download

Download CudaText for OpenBSD from: <https://www.fosshub.com/CudaText.html>

## Step 2: Extract

Assuming you have the downloaded file on `~/Downloads`:

```sh
mkdir -p ~/bin/cudatext-bin
xz -d < ~/Downloads/cudatext-*.tar.xz | tar xvf /dev/stdin -C ~/bin/cudatext-bin
```

By default, OpenBSD version is in ["portable" mode](https://wiki.freepascal.org/CudaText#Location_of_folders_.27settings.27.2C_.27py.27.2C_.27data.27), meaning all setting changes (inside settings, py etc. directories) will be stored in the same directory as the executable. But we don't want that since we are so used to modifying `~/.config/cudatext` and most of the instructions online and even the readme of this project is assumed to respect the `~/.config` directory. For CudaText to use `~/.config` instead of the executable directory, we need to move the `data` and `py` directory to the `~/.config/cudatext` directory:

```sh
mkdir -p ~/.config/cudatext
mv -v ~/bin/cudatext-bin/data ~/.config/cudatext/
mv -v ~/bin/cudatext-bin/py ~/.config/cudatext/
mv -v ~/bin/cudatext-bin/settings_default ~/.config/cudatext/ # so that you can open default settings json files
```

To be able to run `cudatext` and not have to run `~/bin/cudatext-bin/cudatext` every time:

```sh
# run this to determine which shell you use:
echo $SHELL
# if output ends with ksh:
which cudatext 2>/dev/null || echo 'PATH="$PATH:$HOME/bin/cudatext-bin"' >> ~/.kshrc
# if output ends with bash:
which cudatext 2>/dev/null || echo 'PATH="$PATH:$HOME/bin/cudatext-bin"' >> ~/.bashrc
```

The `which` commands above searches to see if `cudatext` binary is already accessible. If it is not already accessible, then it adds `~/bin/cudatext-bin` to `$PATH` so that it can be run from any directory. Please be careful to ensure you enter double angle brackets (`>>`) if manually typing these commands because single angle bracket can ruin existing content of the file.

Now to show CudaText in the application menu:

```sh
mkdir -p ~/.local/share/applications/
ftp https://github.com/Alexey-T/CudaText/raw/master/setup/debfiles/cudatext.desktop -o ~/.local/share/applications/cudatext.desktop
```

Basically we're just saving the contents of [this file](https://github.com/Alexey-T/CudaText/raw/master/setup/debfiles/cudatext.desktop) to `~/.local/share/applications/cudatext.desktop`. Then make it executable and change paths:

```
chmod +x ~/.local/share/applications/cudatext.desktop
# to fix the path
sed -i'' -e "s/Exec=cudatext/Exec=\/home\/$USER\/bin\/cudatext-bin\/cudatext/" ~/.local/share/applications/cudatext.desktop
sed -i'' -e "s/Icon=.*/Icon=\/home\/$USER\/bin\/cudatext-bin\/cudatext-512\.png/" ~/.local/share/applications/cudatext.desktop
```

Now log out and log in again. Then run `cudatext` on terminal to see if it runs. If it doesn't, next step is for you. Otherwise

## Step 3: Solving library issues

When you run `cudatext` on terminal, it may show a message like:

```
ld.so: cudatext: can't load library 'libiconv.so.6.0'
Killed
```

This simply means that a library was updated since CudaText was built from source. If the version is one or two versions lower or greater, there is an easy fix. I searched on the system if there are any `libiconv.so` (omitting the version after `.so`) like this:

```
doas find / -name 'libiconv.so*'
```

It returned:

```
/usr/local/lib/libiconv.so.7.0
```

So, it means that CudaText requires "libiconv.so.6.0", but our system has "libiconv.so.7.0". Difference of one or two in version number doesn't usually matter, so we can just symlink the existing file to the expected one, like so:

```
doas ln -s /usr/local/lib/libiconv.so.7.0 /usr/local/lib/libiconv.so.6.0
```

I ran `cudatext` again and the same kind of message appeared again, but for a different file. This is time it is for "libc.so.95.0":

```
ld.so: cudatext: can't load library 'libc.so.95.0'
Killed
```

The process is simple and like before. I searched with .so name ignoring the version numbers at the end.

```
$ doas find / -name 'libc.so*'
/usr/lib/libc.so.96.1
/usr/share/relink/usr/lib/libc.so.96.1.a
```

So at the end I seem to have run these:

```
doas ln -s /usr/local/lib/libiconv.so.7.0 /usr/local/lib/libiconv.so.6.0
doas ln -s /usr/lib/libc.so.96.1 /usr/lib/libc.so.95.0
doas ln -s /usr/local/lib/libpango-1.0.so.3801.2 /usr/local/lib/libpango-1.0.so.3800.2
```

And eventually the CudaText window showed up on my screen. Great!

When I tried this, the latest version was 1.146.2.0. So when the new versions arrive you may not even see the error messages above. And if you do, follow the same process and you should be ok.

## Step 4: Configuring

When you are able to start cudatext at the end, you may see a message like this in the bottom panel:

```
NOTE: No Python 3 engine found. Python plugins don't work now. To fix this:
* write "pylib__openbsd" to user.json. Read about "pylib" in "Options / Settings-default".
* or use the menu item "Plugins / Find Python library".
Startup: 2880ms, plugins: 0ms ()
```

This is easy to fix. Just click **Plugins - Find Python** library, click OK and select an appropriate version. When asked restart CudaText.

Then continue to install this config to use it.
