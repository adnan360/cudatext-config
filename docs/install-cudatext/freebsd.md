# How to Install CudaText on FreeBSD

Tested on FreeBSD 13.1-RELEASE (amd64) with CudaText 1.171.0.0 (gtk2).

Fortunately FreeBSD has a package for CudaText which makes it easy to install and configure things.

## Step 1: Install CudaText

```
sudo pkg install cudatext-gtk2
```

If you want the Qt5 version you can just run `sudo pkg install cudatext-qt5` instead. This config was only tested on gtk2 version.

## Step 2: Install config

Proceed with install instructions in [project `README.md` file](../../README.md).

## Step 3: Solving the python engine issue

When you launch CudaText after installing the config, you might see a message like the following in the Console panel at the bottom:

```
NOTE: No Python 3 engine found. Python plugins don't work now. To fix this:
* write "pylib__freebsd" to user.json. Read about "pylib" in "Options / Settings-default".
* or use the menu item "Plugins / Python engine was not found; find it..."
```

Just click **Plugins - Python engine was not found; find it...** menu, press enter and when a list shows up press enter again. Then close CudaText and reopen to fix it.
