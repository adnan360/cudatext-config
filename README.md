# cudatext-config

A typical CudaText config

## Install

If you are having trouble installing CudaText on your system, you may check [here](docs/install-cudatext/). Currently guides for only OpenBSD and FreeBSD exists. Please post an issue or PR if you want one for any other system.

**Requirements:**

```
# Arch/Parabola:
sudo pacman -S ttf-jetbrains-mono
# OpenBSD:
doas pkg_add jetbrains-mono
# FreeBSD:
sudo pkg install jetbrains-mono
```

If Jetbrains Mono is not available in your repo, try [downloading it from here](https://www.jetbrains.com/lp/mono/) and extracting it in your `~/.fonts` directory on a Linux/Unix machine. Or if you don't like the look of Jetbrains Mono, feel free to change values in key names starting with `font_name` in `.config/cudatext/settings/user.json`.

**Install config:**

```
cp -r ./.config ~/
```

## Keyboard shortcuts

`.config/cudatext/settings/keys.json` has these:

- Ctrl+/ = Toggle line comment
- Ctrl+K = Delete selected line
- Ctrl+Y = Redo
- Ctrl+Shift+K = Delete to line end
- Ctrl+Shift+L = Lexers (dialog)
- Alt+Shift+U = Place new carets up a line (carets extend: up a line)
- Alt+Shift+D = Place new carets down a line (carets extend: down a line)
- Ctrl+[ = insert empty line above
- Ctrl+] = insert empty line below
- Ctrl+Shift+= = Make font size bigger (all documents)
- Ctrl+- = Make font size smaller (all documents)
- Ctrl+0 = Reset font size (all documents)

## Plugins

You can optionally install plugins and configure them for greater convenience. Use Plugins -> Addons manager -> Install... to install plugin.

Here are some of the plugins that you can try:

**Complete_From_Text:**

Click Options -> Settings - plugins -> Complete From Text and make sure you change the `[complete_from_text]` stanza to this below:

```
[complete_from_text]
lexers=*
min_len=2
case_sens=0
no_comments=0
no_strings=0
what_editors=0
max_lines=10000
use_acp=1
case_split=0
underscore_split=0
```

Now typing 2 letters and pressing Ctrl+Space in any document should give you a list of similar words to autocomplete from within the file.

**Snippets**

Press tab to autocomplete a snippet. e.g. on an HTML file typing "html" and then pressing tab expands it into a basic HTML document snippet and places the cursor on the first editing spots. More editing spots are indicated by arrows, and pressing more tabs will go to next ones. Snippets plugin has some snippets already built in. If more are needed:
- click Plugins -> Addons manager -> Install... and enter "snippet" to see if any other snippet collection is available for you.
- click Plugins -> Snippets -> Install VSCode snippets...

Some of the VSCode snippets that may suit your needs:
- CSS -> VSCode CSS Snippets 0.6.1
- JavaScript, TypeScript -> JavaScript Snippets 4.0.1
- Python -> Python code snippets 1.2.1

**Git Status**

If you are someone who edits files within Git version controlled repos too often, this could be useful to you. This plugin shows the current branch for the currently open file if it is in a Git repositor. It shows this in the status bar below. It also shows an asterisk (*) next to the branch name if there are any modified files waiting to be committed. This is useful when editing files between multiple branches or just to have a clear view of things. Since we're using dark theme on this config, after installing, you can go to Options -> Settings - plugins -> Git Status -> Config and change `white_icon=0` to `white_icon=1`, then restart CudaText.

One of the limitations is that it does not auto-update the status. e.g. If you've committed the changes, it might not remove the asterisk by itself. You may have to change to another tab and come back to it to find the updated status.

## License

CC0 1.0 Universal, unless otherwise stated. See `LICENSE` for details.
